import React, { Component } from 'react';
import * as BooksAPI from '../BooksAPI'

class Book extends Component {

    
    // Idea for e glitch: assign an id and store in a var, use dom to select it as an e replacement
    render(){
        return(
            <li key={this.props.b.industryIdentifiers[0].identifier.toString()}>
                <div className="book">
                    <div className="book-top">
                        <div className="book-cover" style={{ width: 128, height: 193, backgroundImage: `url("${this.props.b.imageLinks.thumbnail}")` }}></div>
                        <div className="book-shelf-changer">
                            <select id="test" value={this.props.b.shelf || ""} onChange={(e) => { this.props.updateBook(this.props.b, e.target.value) }}> 
                                <option value="move" disabled>Move to...</option>
                                <option value="currentlyReading">Currently Reading</option>
                                <option value="wantToRead">Want to Read</option>
                                <option value="read">Read</option>
                                <option value="none">None</option>
                            </select>
                        </div>
                    </div>
                    <div className="book-title">{this.props.b.title}</div>
                    <div className="book-authors">{this.props.b.authors}</div>
                </div>
            </li>
        )
    }
}

export default Book;